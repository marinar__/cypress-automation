## Arquitetura do Mini Projeto - Cypress

- Interpretador Js - [Node.js - 12.16.1](https://nodejs.org/en/)

- Ambiente de desenvolvimento - [Visual Studio Code](https://code.visualstudio.com)

- Linguagem de desenvolvimento - [JavaScript](https://www.javascript.com)

- Ferramenta de testes automatizados - [Cypress - 4.5](http://cypress.io)

- Relatório de teste on-line -[Cypress Dashboard](https://dashboard.cypress.io/)

- Relatório de teste - [Mochawesome - 6.1.1](https://www.npmjs.com/package/mochawesome)

  
## Executar o projeto

 1. Install Node JS 
 2. Install NPM 
 3. Instalar Cypress 4.5 (npm install cypress@4.5)
 4. Install VSCode

Abrir o VSCode com o projeto e no terminal digitar o comando:

    npm install

    npx cypress open

O cypress abrirá com todas as specs (arquivos de testes) do projeto:

Clique no spec que deseje executar os testes e os mesmos serão executados:

Permaneça com a tela aberta do spec que esteja codando e quando você salvar o arquivo no VSCode (ctrl+ s), os testes serão reexecutados automaticamente.

## Configurando Mochawesome:

No terminal:

    npm install mocha --save-dev

    npm install cypress-multi-reporters --save-dev

    npm install mochawesome --save-dev

    npm install mochawesome-merge --save-dev

    npm install mochawesome-report-generator --save-dev

No cypress.json:


    "reporter": "cypress-multi-reporters",
     "reporterOptions": {
     "reporterEnabled": "mochawesome",
    "mochawesomeReporterOptions": {
     "reportDir": "cypress/reports/mocha",
    "quite": true,
     "overwrite": false,
     "html": true,
     "json": true
     }
     }

 No scripts do package.json:

     "scripts": {
     "open": "cypress open",
     "cy": "cypress run",
     "clean:reports": "rmdir /S /Q cypress\\reports && mkdir cypress\\reports && mkdir cypress\\reports\\mochareports",
     "pretest": "npm run clean:reports",
     "combine-reports": "mochawesome-merge cypress/reports/mocha/*.json >cypress/reports/mochareports/report.json",
     "generate-report": "marge cypress/reports/mochareports/report.json -f report -o cypress/reports/mochareports",
     "posttest": "npm run combine-reports && npm run generate-report",
     "test" : "npm run scripts || npm run posttest"
     }

  

Se desejar usar o Dashboard do Cypress utilize o tópico abaixo:


### Para Instalar o cypress: 
  `npm install cypress@3`

 Irá adicionar o package-lock.json e o node_modules ao projeto.

![instalando cypress](https://i.imgur.com/fE3d3pj.png)

  

E agora vamos abrir o projeto pela primeira vez:
 `npx open cypress`

Irá abrir o cypress e inserir a pasta cypress no projeto com a arquitetura padrao e exemplos, como também o cypress.json:

Para executar os testes direto no terminal utilizamos:
  `npx cypress run`



### Para duvidas e sugestoes
marinarosasilva3@gmail.com