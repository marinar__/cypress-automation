//**********Mapeamento de elementos**********
const el = {
  menuLateral: "//a[@id='menu-icon']",
  labelBusca: "//input[@title='Search']",
  listaProdutos: "//div[@id='loops-wrapper']",
  msgProdutoInexistente: "//div[@id='content']",
  menuShop: "//li[@id='menu-item-40']",
  myAccount:"//a[contains(text(),'My Account')]",
  }
  
  //**********Ações**********

  class BuscaPage{

    expandeMenuLateral(){
      cy.xpath(el.menuLateral).click()
    }
    digitarProduto(produto){
        cy.xpath(el.labelBusca).type(produto)
        .type('{enter}')
    }
    verificarProdutoHTML(param){
    cy.xpath(el.listaProdutos).should('not', param)
    }
    verificaProdutoInexistente(param){
      cy.xpath(el.msgProdutoInexistente).should('contain', param)
    }
    clicaMenuShop(){
      cy.xpath(el.menuShop).click()
    }
    clicarMyAccount(){
      cy.xpath(el.myAccount).click()
    }
  
  }
  
  export default new BuscaPage()
  