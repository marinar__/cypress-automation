//**********Mapeamento de elementos**********
const el = {
    emailLabel: "//input[@id='reg_email']",
    senhaLabel: "//input[@id='reg_password']",
    registerButton: "//input[@value='Register']",
    userRegister:"//p[contains(text(),'Hello')]"
    }
    
    //**********Ações**********
  
    class MyAccountPage{
  
        preencheEmail(email){
            cy.xpath(el.emailLabel).type(email)
        }

        preencheSenha(password){
            cy.xpath(el.senhaLabel).type(password)

        }
        clicarRegister(){
            cy.xpath(el.registerButton).click()
         }
         verificaUsuarioRegistrado(param){
            cy.xpath(el.userRegister).should('contain', param)
        }

}  
    
    
    export default new MyAccountPage()