//**********Mapeamento de elementos**********
const el = {
    categoriaHTML: "//a[contains(text(),'HTML')]",
    listarProdutos: "//ul[@class='products masonry-done']"
    }
    
    //**********Ações**********
  
    class ShopPage{
  
    clicaCategoriaHTML(){
        cy.xpath(el.categoriaHTML).click()
        }

    verificaProdutoLista(param){
    cy.xpath(el.listarProdutos).should('contain', param)
    }

    
    }
    
    export default new ShopPage()
    