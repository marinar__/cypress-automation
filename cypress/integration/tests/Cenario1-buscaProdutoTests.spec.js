///<reference types="Cypress"/>

//Import de Page
import BuscaPage from '../../support/pages/BuscaPage.js'


describe('Busca de um produto específico', () => {
  //uso o beforeEach p este iniciar antes de cada teste (it)
  const expectedProduto = 'Android Quick Start'
  beforeEach(() => {
    cy.visit(Cypress.config('url'))
  })
  //Parametros (Arrange)
  const produto = Cypress.config('produto')

    it('Busca produto HTML', () => {
    BuscaPage.expandeMenuLateral()
    BuscaPage.digitarProduto(produto)
    BuscaPage.verificarProdutoHTML(expectedProduto)
  })


})
