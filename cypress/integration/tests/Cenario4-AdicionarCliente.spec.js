///<reference types="Cypress"/>

//Import de Page
import BuscaPage from '../../support/pages/BuscaPage.js'
import MyAccountPage from '../../support/pages/MyAccountPage.js'

describe('Criação de conta de usuário', () => {
  //uso o beforeEach p este iniciar antes de cada teste (it)
  const expectedProduto = 'Android Quick Start'
  beforeEach(() => {
    cy.visit(Cypress.config('url'))
  })
  //Parametros (Arrange)
  const email = Cypress.config('email')
  const password = Cypress.config('password')
  const msgUserLogado = Cypress.config('msgUserLogado')

    it('Usuário novo', () => {
    BuscaPage.expandeMenuLateral()
    BuscaPage.clicarMyAccount()
    MyAccountPage.preencheEmail(email)
    MyAccountPage.preencheSenha(password)
    MyAccountPage.clicarRegister()
    MyAccountPage.verificaUsuarioRegistrado(msgUserLogado)
    
  })


})