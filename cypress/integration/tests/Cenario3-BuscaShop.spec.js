///<reference types="Cypress"/>

//Import de Page
import BuscaPage from '../../support/pages/BuscaPage.js'
import ShopPage from '../../support/pages/ShopPage.js'

describe('Busca pela guia shop e verificaçao se o produto existe na lista', () => {
  //uso o beforeEach p este iniciar antes de cada teste (it)
  const expectedProduto = 'Android Quick Start'
  beforeEach(() => {
    cy.visit(Cypress.config('url'))
  })
  //Parametros (Arrange)
  const verificaProduto = Cypress.config('produtoNaLista')

    it('Busca um produto pelo menu Shop', () => {
    BuscaPage.expandeMenuLateral()
    cy.wait(3000)
    BuscaPage.clicaMenuShop()
    cy.wait(3000)
    ShopPage.clicaCategoriaHTML()
    cy.wait(3000)
    ShopPage.verificaProdutoLista(verificaProduto)
    cy.wait(3000)

  })


})
