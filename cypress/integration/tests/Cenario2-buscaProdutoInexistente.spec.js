///<reference types="Cypress"/>

//Import de Page
import BuscaPage from '../../support/pages/BuscaPage.js'


describe('Busca de um produto Inexistente', () => {
  //uso o beforeEach p este iniciar antes de cada teste (it)
  const expectedMensagemProdutoInexistente = 'Sorry, nothing found'

  beforeEach(() => {
    cy.visit(Cypress.config('url'))
  })
  //Parametros (Arrange)
  const produtoInexistente = Cypress.config('produtoInexistente')

  it('Busca produto Inexistente', () => {
    BuscaPage.expandeMenuLateral()
    BuscaPage.digitarProduto(produtoInexistente)
    BuscaPage.verificaProdutoInexistente(expectedMensagemProdutoInexistente)
  })
})